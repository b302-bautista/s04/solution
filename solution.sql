INSERT INTO artists (name) VALUES 
("Tayloy Swift"), 
("Lady Gaga"), 
("Justin Bieber"), 
("Ariana Grande"), 
("Bruno Mars");
/* 
Taylor Swift - id 3
Lady Gaga - id 4
Justin Bieber - id 5
Ariana Grande - id 6
Bruno Mars - id 7
*/
INSERT INTO albums (album_title, date_released, artist_id)  VALUES ("Fearless", "2008-01-01", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Fearless", 246, "Pop Rock", 3),
	("Love Story", 213, "Country pop", 3);

INSERT INTO albums(album_title, date_released, artist_id) VALUES 
	("Red", "2012-01-01", 3);
/* album id-4 */
INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("State of Grace", 250, "Rock, alternative rock, areana rock", 4), 
	("Red", 204, "Country", 4);

/* Lady Gaga */

INSERT INTO albums (album_title, date_released, artist_id) VALUES 
	("A Star is Born", "2018-01-01", 4);  --5

INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Black eyes", 181, "Rock and Roll", 5), 
	("Shallow", 201, "Country, rock, folk rock", 5);
--6
INSERT INTO albums(album_title, date_released, artist_id) VALUES 
	("Born This Way", "2011-01-01", 4 );

INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Born This Way", 252, "Electropop", 6 );

-- Justin Bieber (4)
-- album id 6

INSERT INTO albums(album_title, date_released, artist_id) VALUES 
	("Purpose", "2015-01-01", 5 );

INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Sorry", 200, "Dancehall--poptropical housemoombathon", 7 );

----------------------------------
/* ADVANCE SELECTS */

-- Excluding records

SELECT * FROM songs WHERE id !=1;

-- Greater than, less than, or equal
SELECT * FROM songs WHERE id > 1;
SELECT * FROM songs WHERE id < 9;
SELECT * FROM songs WHERE id = 1;

-- OR
SELECT * FROM songs WHERE id = 1 OR id = 5;

-- IN
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("OPM", "Electropop");

-- Combining Conditions
SELECT * FROM songs WHERE genre = "OPM" and length > 230;

-- Find partial matches
-- ENDS WITH
SELECT * FROM songs WHERE song_name LIKE "%a"; -- select keyword from the end of word

-- STARTS WITH
SELECT * FROM songs WHERE song_name LIKE "b%"; -- select keyword from the start of word

-- SELECT keyword in between
SELECT * FROM songs WHERE song_name LIKE "%b%" AND song_name LIKE "______Eyes"; 

SELECT * FROM albums WHERE date_released LIKE "201_-__-__";

SELECT * FROM songs WHERE song_name LIKE "%a%";
-- Finding song_name with second character e
SELECT * FROM songs WHERE song_name LIKE "%_e%";

-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- DISTINCT
SELECT DISTINCT genre FROM songs;

-- Table Joins
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/*SELECT* FROM ReferenceTable JOIN tableName ON ReferenceTable.PrimaryKey = TableName.foreignKey */

SELECT * FROM albums
	JOIN artists ON albums.artist_id = artists.id;

-- Combine more than two Tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;
/* SELECT* FROM table1
	JOIN table2 ON table1.primarykey = table2.foreignkey
	JOIN table3 ON table2.primarykey = table3=foreignkey */


SELECT songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included per table
SELECT artists.name, albums.album_title FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/* UNION */
SELECT artist.name FROM artists UNION SELECT albums.album_title FROM albums

/* INNER JOIN */
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id

/* LEFT JOIN */
SELECT * FROM artists
	LEFT JOIN albums ON artist.id = albums.artist_id;

/* RIGHT JOIN */
SELECT * FROM albums
	RIGHT JOIN artists ON albums.artist_id = artists.id;



-- 1. Find all artists that has letter d in its name.

SELECT * FROM artists WHERE name LIKE "%d%";

-- 2. Find all songs that has a length of less than 3:50

SELECT * FROM songs WHERE length < 350;

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)



SELECT  albums.album_title, songs.song_name, songs.length  FROM albums JOIN songs ON albums.id = songs.album_id;


-- 4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
/* SELECT* FROM table1
	JOIN table2 ON table1.primarykey = table2.foreignkey
	JOIN table3 ON table2.primarykey = table3=foreignkey */


SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE '%a%';

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;




-- 6. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)



SELECT albums.id, albums.album_title, albums.date_released, albums.artist_id,  songs.id, songs.song_name, songs.length, songs.genre, songs.album_id
FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC;




